SaveExportFileAs(checkBoxString, typeEnding, activeId, fileInput, newName, SLEEP_TIMER, saveExport, itemIndex, addParam) {
	
	
	WinActivate, ahk_id %activeId%
	;MsgBox % "InFileSaveAs " typeEnding
	
	Sleep, %SLEEP_TIMER%

	Sleep, %SLEEP_TIMER%

	Send, !f
	Sleep, %SLEEP_TIMER%


	
	if(saveExport = "s"){
		keyword = Save
		Send, a
	}
	else{
		keyword = Export
		Send, e
	}

	Sleep, %SLEEP_TIMER%
	WinWait, %keyword%
	WinGetTitle, title, %keyword%
	Send !t
	Sleep, %SLEEP_TIMER%

	;sgBox, b

	ControlGetFocus, TypeBox, %title%
	Sleep, %SLEEP_TIMER%

	;MsgBox, c

	Control, ChooseString, %checkBoxString%, %TypeBox%, %title%
	Sleep, %SLEEP_TIMER%
	
	;MsgBox, d
	
	;StringReplace, typeEndingNew, typeEnding, ..,., All
	; [\w\s]+(?=\()
	
	if(addParam){
		
		PreText := ""
		; RegExMatch(checkBoxString, "[\w\s\d-_\+]+(?=\()", PreText, 1)
		RegExMatch(checkBoxString, "[^(]*", PreText, 1)
		WriteLog("Got PreText: " . PreText . " for ending: " . typeEnding . " checkboxText: " . checkBoxString) 
		ReplacedStr := RegExReplace(PreText, "\s", "_")
		
		StringRight, LastChar, ReplacedStr, 1
		
		if(LastChar =="_"){
			StringTrimRight, ReplacedStr, ReplacedStr, 1  
		}
		
		if(itemIndex ==""){
			storePath := newName .  "_" . ReplacedStr . typeEnding
		}
		else{
			storePath := newName .  "_" . ReplacedStr . "_" . itemIndex . typeEnding
		}
	}
	else{
		storePath := newName . typeEnding 
	}
	
	ControlSetText, Edit1, %storePath%, %title% ;TODO check if this is always edit1
	
	;MsgBox, e

	
	Sleep, %SLEEP_TIMER%
	
	WinGet, OldId, ID, %keyword%
	;MsgBox % "Before: " OldId
	
	SetControlDelay -1
	ControlClick,&Save,%title% ;TODO save always the right button?
	SetControlDelay 50
	Sleep, %SLEEP_TIMER%
	Sleep, %SLEEP_TIMER%

	
	WinGet, NewId, ID, A
	;MsgBox % "After: " NewId
	; TODO just do an active window id check here

	If(NewID = activeId)
	{
		;MsgBox "id was blank, no overwrite needed"
		Sleep, %SLEEP_TIMER%
		return
	}
	else ;TODO do this in loop?
	{
		WinGetTitle, newTit, A
		WriteLog("Found new window after saving (for example, overwrite or other warning), titled: " . newTit)
		WinGet, ActiveControlList, ControlList, A
		Loop, Parse, ActiveControlList, `n
		{
			ControlGetText, ctrlText, %A_LoopField%, A
			WriteLog("Got Text on Control: " . ctrlText)
			
			confirm_list = ok,&ok,yes,&yes,confirm,continue,&confirm,&continue,c&onfirm,c&ontinue
			if ctrlText in %confirm_list%
			{
				WriteLog("Found word from confirm list for control: " . A_LoopField . " (" . ctrlText . ").")
				Sleep, %SLEEP_TIMER%

				SetControlDelay -1
				ControlClick, %A_LoopField%, A
				SetControlDelay 50
				Sleep, %SLEEP_TIMER%
				return
			}
		}

		WriteLog("Did not find control with text from the confirm list, defaulting to Enter...")
		Send, {Enter} ;Default to pressing enter (might not work, if needed append more things to confirm list
	}
	
	
	Sleep, %SLEEP_TIMER%
	return 
}
