#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%
#SingleInstance,Force
#WinActivateForce
SetControlDelay 50
SetKeyDelay 20
SetTitleMatchMode 2
DetectHiddenWindows, OFF


StartProgram(){
	;TODO logs?
	
	global logfile
	WriteLog("Checking for __eaas_autostart.txt...")

	Sleep, 2000

	DriveGet, AllDrives, List ;, CDROM
		Loop, Parse, AllDrives
		{
			fileName = %A_LoopField%:\__eaas_autostart.txt

			if FileExist(fileName){
				
				WriteLog("Found __eaas_autostart.txt") 

				FileRead, ToBeExe, %fileName%
				SetWorkingDir %A_LoopField%:\
				
				WriteLog("Executing: " . ToBeExe)

				RunWait %ToBeExe%
				
				Shutdown, 1
			}
		}
	Sleep, 2000
}

WriteLog(msg){
	global logfile
	FormatTime, TimeString, %A_Now%,yyyy-MM-d hh:mm:ss tt
	FileAppend, %TimeString%: %msg% `n, %logfile% 

}

logfile = C:\autostart_log.txt

WriteLog("Starting...")
WriteLog("Sleeping for 15 Seconds")
Sleep, 10000

WriteLog("Checking 64 or 32 bit")

if (A_Is64bitOS){
	WriteLog("Found 64 bit system")
	SetRegView, 64
	
}
else{
	WriteLog("Found 32 bit system") 
	SetRegView, 32
}

RegRead, isActive, HKLM, SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon, AutoAdminLogon
toSet := "1"

if(isActive){
	toSet := "0"
	WriteLog("Autologin already enabled, will execute program...")

}

RegWrite, REG_SZ, HKLM, SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon, AutoAdminLogon, %toSet%
RegWrite, REG_SZ, HKLM, SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon, DefaultUserName, %1%
RegWrite, REG_SZ, HKLM, SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon, DefaultPassword, %2%
;RegWrite, REG_SZ, HKLM, SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon, DefaultDomainName, OPENSLX-AH078G7

if(isActive){
	WriteLog("Logged in user: " . A_UserName . " Will now execute program...") 
	StartProgram()
}
else{
	Shutdown, 2
}

^q::
WriteLog("Ctrl + Q was pressed, terminating!")
ExitApp, 0
