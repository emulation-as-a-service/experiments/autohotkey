
openFile(fileInput, activeId, SLEEP_TIMER){
	Sleep, %SLEEP_TIMER%

	WinActivate, ahk_id %activeId%
	Sleep, %SLEEP_TIMER%
	Sleep, %SLEEP_TIMER%
	
	WinGetActiveTitle, openActiveTitle
	WriteLog("Opening with active Window: " . openActiveTitle)

	Send, !f
	Sleep, %SLEEP_TIMER%
	Send, o
	Sleep, %SLEEP_TIMER%
	WinWait, Open,,10
	if(ErrorLevel){
		WriteLog("Could not find Window with 'Open' in the title, trying 'Select'...")
	}
	else{
		WinWait, Select,,10
		if(ErrorLevel){
			WriteLog("Could not find Window with 'Select' in the title, continuing anyway...")
		}
	}

	WinGetTitle, openTitle, A
	Sleep, %SLEEP_TIMER%
	ControlGetFocus, openBox, %openTitle%
	Sleep, %SLEEP_TIMER%
	ControlSetText, %openBox%, %fileInput%, %openTitle%
	Sleep, %SLEEP_TIMER%
	SetControlDelay -1
	ControlClick,&Open,%openTitle%
	SetControlDelay 50

	Sleep, %SLEEP_TIMER%
}