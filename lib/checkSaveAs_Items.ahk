WriteLog("Checking export types...")

; Initial check for filetypes
Sleep, %SLEEP_TIMER%

Send, !f
Sleep, %SLEEP_TIMER%

if(saveExport = "s"){
	keyword = Save
	Send, a
}
else{
	keyword = Export
	Send, e
}

Sleep, %SLEEP_TIMER%


WinGetTitle, title, %keyword%
WriteLog("Got Save/Export Title: " . title)
Sleep, %SLEEP_TIMER%
WriteLog("Sending alt t to " . title)
Send,!t ; used to be controlsend to title, however this does not seem to work for some programs (for whatever win9X reason...)
Sleep, %SLEEP_TIMER%
ControlGetFocus, TypeBox, %title%
WriteLog("Focus after alt t: " . TypeBox)
Sleep, %SLEEP_TIMER%
; TODO if no focus, retry with active window? use active window generally (using non active window is not necessary due to windows xp autologin stuff anymore)
ControlGet, Items, List,,%TypeBox%, %title%
Sleep, %SLEEP_TIMER%
WinClose, %keyword%