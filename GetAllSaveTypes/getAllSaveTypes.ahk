#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%
#SingleInstance,Force
#WinActivateForce
SetControlDelay 50
SetKeyDelay 20
SetTitleMatchMode 2
DetectHiddenWindows, OFF

#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%
#SingleInstance,Force
#WinActivateForce
SetControlDelay 50
SetKeyDelay 20
SetTitleMatchMode 2
DetectHiddenWindows, OFF

logfile = C:\save_types_log.txt
resultfile = C:\all_file_types.txt
FileDelete %resultfile%
FileDelete %logfile%
#Include ../lib/logging.ahk

WriteLog("Starting All Types Exporter (With Opening example file)")

prog = %1%
fileInput = %2%
saveExport = %3%

SLEEP_TIMER = %4%

INITIAL_DELAY = %5%

WriteLog("Got inputs:" prog . " " . fileInput . " " . storeDirectory . " " . saveExport . " " . SLEEP_TIMER . " " . INITIAL_DELAY)

; SLEEP_TIMER = 2200

IfInString, fileInput, :\
{
	WriteLog("Input was given as absolut path: " .  fileInput)
}
else{
	fileInput = %A_ScriptDir%\%fileInput%
	WriteLog("Input was given as relative path. WorkingDir will be appended: " . fileInput)
	
}

SplitPath, fileInput,,dir,,baseName

#Include ../lib/checkActiveWindows_activeId.ahk

Sleep, %SLEEP_TIMER%

#Include ../lib/openFile.ahk
openFile(fileInput, activeId, SLEEP_TIMER)

Sleep, %SLEEP_TIMER%
Sleep, %SLEEP_TIMER%

#Include ../lib/checkSaveAs_Items.ahk

WriteLog("Starting Main Program...")

; Main Loop
Loop, Parse, Items, `n 
{
	WriteLog("Item: " . A_LoopField)
	FileAppend, %A_LoopField% `n, %resultfile%
}

Sleep 1000

WriteLog("Closing Window...")

WinClose, ahk_id %activeId% 
WriteLog("Exiting...")

ExitApp 0


^Y::
WriteLog("Ctrl + Y was pressed, terminating converter!")
ExitApp, 0
	

	


