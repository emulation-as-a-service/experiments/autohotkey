#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%
#SingleInstance,Force
#WinActivateForce
SetControlDelay 50
SetKeyDelay 20
SetTitleMatchMode 2
DetectHiddenWindows, OFF


Sleep, 8000


logfile = C:\logfile.txt
FileAppend, Starting... running notepad...  `n, %logfile% 

Run notepad

Sleep, 2000

WinGetActiveTitle, Title
FileAppend, The active window is "%Title%".`n, %logfile% 

FileAppend, Manually activating... `n

WinActivate notepad
Sleep, 2000

FileAppend, The active window is "%Title%".`n, %logfile% 

ExitApp, 0