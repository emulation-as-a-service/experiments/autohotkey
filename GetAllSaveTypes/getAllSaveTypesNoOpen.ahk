#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%
#SingleInstance,Force
#WinActivateForce
SetControlDelay 50
SetKeyDelay 20
SetTitleMatchMode 2
DetectHiddenWindows, OFF

;prog = C:\Program Files (x86)\Notepad++\notepad++.exe
;prog = C:\Program Files (x86)\Sublime Text 3\sublime_text.exe
;fileInput = C:\config.txt

logfile = C:\save_types.txt
FileDelete %logfile%
#Include ../lib/logging.ahk

WriteLog("Starting All Types Exporter (No Open)")

prog = %1%
saveExport = %2%

SLEEP_TIMER = 1500

#Include ../lib/checkActiveWindows_activeId.ahk

Sleep, %SLEEP_TIMER%

#Include ../lib/checkSaveAs_Items.ahk

WriteLog("Listing all save as/export options:")
WriteLog("")

; Main Loop
Loop, Parse, Items, `n 
{
	WriteLog(A_LoopField) 
}

Sleep 1000
WinClose, ahk_id %activeId% ; TODO force this? autocad for e.g. thinks there are changes after export -> press yes/no
ExitApp 0


^q::
WriteLog("Ctrl + Q was pressed, terminating converter!")
ExitApp, 0
	

	


