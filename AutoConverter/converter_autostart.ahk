#NoEnv
;SendMode Input
SetWorkingDir %A_ScriptDir%
#SingleInstance,Force
#WinActivateForce
SetControlDelay 200
SetKeyDelay 500
SetTitleMatchMode 2

DetectHiddenWindows, OFF

prog = %1%
file = %2%
convertType = %3%
fileEnding = %4%

Run %prog% %file%
Sleep 1000

WinGet windows, List
Loop %windows%
{
	id:= windows%A_Index%
	WinGetTitle wt, ahk_id %id%
	IfInString, wt, notepad
	{
		notepadId = id
		notepadTitle = %wt%
	}
}

Sleep 2000

WinMenuSelectItem, %notepadTitle%,,File, Save As

Sleep 2000

WinGetTitle, tit, Save As
ControlSend,,!t,%tit%
Sleep 2500

ControlGetFocus, TypeBox, %tit%
Sleep 2000
ControlGet, Items, List,,%TypeBox%, %tit%

Control, ChooseString, %convertType%, %TypeBox%, %tit%
Sleep 2000

if (ErrorLevel) 
{
	ControlGet, typeText, Choice, , %TypeBox%, %tit%
	IfInString, typeText, %convertType%
	{
		;MsgBox, "Successfully set type to" %typeText%
	}
	else
	{
		ExitApp, 1
	}
}

Sleep 2000

SplitPath, file,,dir,,basename


If (fileEnding =""){ ; TODO use dir?
	newname :=  "C" . "+{vkBA}" . "{vkDC}" . "AHKAutostart" . "{vkDC}" . basename
	; if no file type is specified just store without old ending
}
else{
	newname :=  "{vkDC}" . "ahkautostart" . "{vkDC}" . basename . "." . fileEnding 
	;newname :=  "C" . "+{vkBA}" . "{vkDC}" . "AHKAutostart" . "{vkDC}" . basename . "." . fileEnding 
	; when a type is specified, use it (necessary for programs, where one type option still allows for multiple types)
	
	ControlSend,Edit1,{Shift down}, %tit%
	ControlSend,Edit1,C, %tit%
	ControlSend,Edit1,{vkBA}, %tit%
	ControlSend,Edit1,{Shift up}, %tit%
	ControlSend,Edit1,%newname%, %tit%
	
}

Sleep 1000




Sleep 1500
ControlSend,,!s,%tit%
Sleep 1500

ExitApp 0













