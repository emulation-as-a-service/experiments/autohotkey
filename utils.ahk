
; this is probably not necessary, because we use autologin for Windows XP now
; however, manually getting the id of the new window assures that it gets activated properly
; Get all windows IDs before executing program

WriteLog("Window Titles before opening:")
AllIds := []
WinGet windowsOld, List
Loop %windowsOld%
{
	oldId := windowsOld%A_Index%
	AllIds%A_Index% := oldId
	
	WinGetTitle wt, ahk_id %oldId%
	WriteLog("WinID: " . oldId . " Title: " . wt) 
}

WriteLog("Executing Program at " . prog)
Sleep 1000
Run %prog%
Sleep 3000 ;TODO maybe check in loop until new window is available (might be necessary for older/slower OS, or complex programs)
	
; get the ID of the newly opened Window
WinGet windows, List
Loop %windows%
{
	idNew := windows%A_Index%
	WinGetTitle wt, ahk_id %idNew%
	if WindowAlreadyExists(idNew, windowsOld){
		WriteLog("(Old) WinID: " . idNew . " Title: " . wt) 
	}
	else{
		WriteLog("Found new Window with WinID: " . idNew . ", Title: " . wt ) 
		activeId = %idNew%
		activeTitle = %wt%
		break
	}
}

WindowAlreadyExists(winId, len) {
	
	global AllIds
	WinGetTitle nt, ahk_id %winId%
	;MsgBox % "id:" AllIds%A_Index%
	Loop %len%
	{
		if (AllIds%A_Index% = winId){
			return A_Index
		}
	}
	return 0
}
