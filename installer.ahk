#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%
#SingleInstance,Force
	
SetControlDelay 0

; TODO arg
Run *RunAs mingw-w64-install.exe

Sleep 3000 ;Initial delay to ensure window is active 
;TODO check for previously active window, only continue if changed
isDone := false

while(not isDone){
	
	WinGetActiveTitle, Title
	Console.log("Title:", Title)
	WinGet, CList, ControlList, A
	Console.log("List:", CList)
	
	Loop, Parse, CList, `n
	{
		Console.log("Control:", A_LoopField)
		ControlGetText, CText, %A_LoopField%, A
		Console.log("Text:", CText)
		
		If InStr(A_LoopField, "Button"){
			
			Console.log("Found Button")
		
			If InStr(CText, "next", false) or InStr(CText, "install", false){
				Console.log("Found Next button", A_LoopField)
				
				ControlGet, IsEnabled, Enabled,, %A_LoopField%
				If IsEnabled{
					ControlClick, %A_LoopField%,A,,,, NA
					Console.log("Klicked Next Button")
				}
				else{
					;TODO check if other buttons (send enter)
					;if not, wait n times (might be installing something)
				}
				
		
			}
			
			If InStr(CText, "done", false) or InStr(CText, "finish", false){
				Console.log("Found Done button", %A_LoopField%, A_LoopField)
			
				ControlClick, %A_LoopField%,A,,,, NA
				Console.log("Klicked Finish Button")
				isDone := true
			}
			
		}
	}

	Sleep 2000

}
				
