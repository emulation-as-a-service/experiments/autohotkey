#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%
#SingleInstance,Force
#WinActivateForce
SetControlDelay 50
SetKeyDelay 20
SetTitleMatchMode 2
DetectHiddenWindows, OFF


;Need all this ugly stuff, because we can't work with active windows
HasVal(needle, len) {
	
	WinGetTitle nt, ahk_id %needle%
	;MsgBox % "In has Val: " . nt
	Loop %len%
	{
		;MsgBox % "Comparing: " AllIds%A_Index% . " with " . needle
		if (AllIds%A_Index% = needle){
			;MsgBox, "Found Match"
			return A_Index
		}
	}
	;MsgBox,"Found New Window!!!!"
	return 0
}

prog = %1%
file = %2%
convertType = %3%
fileEnding = %4%


;necessary, so we can find the newest Window after we run %prog%
; Docs say id := WinExist() could work, but probably not for this version
AllIds := []
WinGet windowsOld, List
Loop %windowsOld%
{
	oldId := windowsOld%A_Index%
	AllIds%A_Index% := oldId
}

Sleep 1000
Run %prog% %file%
Sleep 2500


WinGet windows, List
Loop %windows%
{
	idNew := windows%A_Index%
	
	;MsgBox % "WindowsOld before function: " . windowsOld
	if hasVal(idNew, windowsOld){
		continue
	}
	else{
		WinGetTitle wt, ahk_id %idNew%
		activeId = %idNew%
		activeTitle = %wt% ;todo rename to active title
		break
	}
}

Sleep 2000

WinMenuSelectItem, %activeTitle%,,File, Save As

Sleep 2000

WinGetTitle, tit, Save As
ControlSend,,!t,%tit%
Sleep 2500

ControlGetFocus, TypeBox, %tit%
Sleep 2000
ControlGet, Items, List,,%TypeBox%, %tit%

Control, ChooseString, %convertType%, %TypeBox%, %tit%
Sleep 2000

if (ErrorLevel) 
{
	ControlGet, typeText, Choice, , %TypeBox%, %tit%
	IfInString, typeText, %convertType%
	{
		;MsgBox, "Successfully set type to" %typeText%
	}
	else
	{
		ExitApp, 1
	}
}

Sleep 2500

SplitPath, file,,dir,,basename


If (fileEnding =""){
	; if no file type is specified just store without old ending
	newname :=  "C:\AHKAutostart\" . basename 
}
else{
	; when a type is specified, use it (necessary for programs, where one type option still allows for multiple types)
	newname := "C:\AHKAutostart\" . basename  . "." . fileEnding 	
}

ControlSetText, Edit1, %newname%, %tit%
Sleep 2500
SetControlDelay -1
ControlClick,&Save,%tit%
Sleep 3500

WinClose, ahk_id %activeId%
ExitApp 0









