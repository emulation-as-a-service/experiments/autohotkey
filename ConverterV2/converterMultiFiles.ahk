#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%
#SingleInstance,Force
#WinActivateForce
SetControlDelay 50
SetKeyDelay 20
SetTitleMatchMode 2
DetectHiddenWindows, OFF


prog = %1%
outputFormat = %2%
storeDirectory = %3%
saveExport = %4% ;TODO remove this and check both menus for type?
SLEEP_TIMER = %5%
INITIAL_DELAY = %6%
fileParameter = %7%

logfile = C:\migration_log.txt
FileDelete %logfile%
#Include ../logging.ahk

WriteLog("Starting Migration Tool") 
; TODO 2 options, launch via cmd and via open?

IfNotExist, %storeDirectory%
	FileCreateDir, %storeDirectory%

StringRight, toCheck, storeDirectory, 1

if(!(toCheck="\")){
storeDirectory := storeDirectory . "\"
}
	

#Include ../lib/checkActiveWindows_activeId.ahk
#Include ../lib/openFile.ahk

Loop, USERDATA\*.*
{
	
	fileInput = %A_ScriptDir%\%A_LoopFileFullPath%
	SplitPath, fileInput,,dir,,baseName

	newName :=  storeDirectory . baseName 
	
	WriteLog("Opening file: " . fileInput) 

	openFile(fileInput, activeId, SLEEP_TIMER)


	Sleep, %SLEEP_TIMER%
	Sleep, %SLEEP_TIMER%

    #Include ../lib/checkSaveAs_Items.ahk

	FoundMatch := false
	
	dot = .

	Loop, Parse, Items, `n 
	{
		if(FoundMatch){
			WriteLog("File already found, going to next...")
			break
		}
		; TODO DOT thing, check for PNG xxx instead of .png
		;MsgBox %A_LoopField%
		IfInString, A_LoopField, %dot%
		{
			
			if (!(fileParameter = ""))
			{
				IfInString, A_LoopField, %fileParameter%
				{
					WriteLog("Found " . fileParameter . " in " . A_LoopField)
				}
				else
				{
					continue
				}
			
			}
			
			
			SearchString := A_LoopField
			FoundPos := 0
			Length := StrLen(SearchString)

			while (FoundPos < Length){
				FoundPos++
				FoundPos := RegExMatch(SearchString, "\.[\w-\.]+", MatchType, FoundPos)
				
				if (FoundPos == 0){
					break
				}
				else if (MatchType = outputFormat){
					FoundMatch := true
					SaveExportFileAs(SearchString, MatchType, activeId, fileInput, newName, SLEEP_TIMER, saveExport, "", true)
				}
			}
		}
		else
		{
			WriteLog("A '.' was not found in " . A_LoopField . ". Will try first 3 letters...")
			StringLeft, typeEnding, A_LoopField, 3
			MatchType :=  "." . typeEnding 
		
			IfInString, outputFormat, %typeEnding%
			{
				WriteLog("Found match, passing: " . MatchType)
				FoundMatch := true
				SaveExportFileAs(A_LoopField, MatchType, activeId, fileInput, newName, SLEEP_TIMER, saveExport, "", false)
			}
		}
	}

	if (!FoundMatch)
	{
		WriteLog("Did not find matching type - trying to save as All Files")
		SaveExportFileAs("All", outputFormat, activeId, fileInput, newName, SLEEP_TIMER, saveExport, "", false)
	}
	
	Sleep, %SLEEP_TIMER%

}

Sleep, %SLEEP_TIMER%

WriteLog("Closing Window...") 

WinClose, ahk_id %activeId% ; TODO force this? autocad for e.g. thinks there are changes after export -> press yes/no
WriteLog("Exiting...") 

ExitApp 0

#Include ../lib/saveExportFile.ahk

^y::
WriteLog("Ctrl + Y was pressed, terminating converter!")
ExitApp, 0
	


