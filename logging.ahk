WriteLog(msg){
	global logfile
	FormatTime, TimeString, %A_Now%,yyyy-MM-d hh:mm:ss tt
	FileAppend, %TimeString%: %msg% `n, %logfile%
}