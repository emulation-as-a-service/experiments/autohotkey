#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%
#SingleInstance,Force
#WinActivateForce
SetControlDelay 50
SetKeyDelay 20
SetTitleMatchMode 2
DetectHiddenWindows, OFF

SLEEP_TIMER = 1500

prog = %1%
fileInput = %2%
outputFormat = %3%

logfile = C:\migration_log.txt
FileDelete %logfile%
#Include ../lib/saveExportFile.ahk
WriteLog("Starting Migration Tool")

IfInString, fileInput, :\
{
	FileAppend, %A_Now%: Input was given as absolut path: %fileInput% `n, %logfile% 
}
else{
	FileAppend, %A_Now%: Input was given as relative path. WorkingDir will be used: %A_ScriptDir%:\%fileInput% `n, %logfile% 
	fileInput = %A_ScriptDir%\%fileInput%
}


SplitPath, fileInput,,dir,,baseName

storeDirectory = %4%
if(!(storeDirectory="")){
	
	IfNotExist, %4%
		FileCreateDir, %4%
	
	StringRight, toCheck, storeDirectory, 1
	
	if(!(toCheck="\")){
	storeDirectory := storeDirectory . "\"
	}
	
	newName :=  storeDirectory . baseName 
}
else{
	newName := baseName
}


FileAppend, %A_Now%: Window Titles before opening: `n, %logfile% 


; Get all windows IDs before executing program
AllIds := []
WinGet windowsOld, List
Loop %windowsOld%
{
	oldId := windowsOld%A_Index%
	AllIds%A_Index% := oldId
	
	WinGetTitle wt, ahk_id %oldId%
	FileAppend, %A_Now%: WinID: %oldId% Title: %wt% `n, %logfile% 
}

FileAppend, `n%A_Now%: Executing Program at %prog%... `n`n, %logfile% 
Sleep %SLEEP_TIMER%
Run %prog%
Sleep 15000 ;TODO maybe check in loop until new window is available (might be necessary for older/slower OS, or complex programs)

; get the ID of the newly opened Window
; I didn't find a better solution for this AHK version and without using active window
WinGet windows, List
Loop %windows%
{
	idNew := windows%A_Index%
	WinGetTitle wt, ahk_id %idNew%
	if WindowAlreadyExists(idNew, windowsOld){
		FileAppend, %A_Now%: (Old) WinID: %idNew% Title: %wt% `n, %logfile% 
	}
	else{
		FileAppend, %A_Now%: Found new Window: WinID: %idNew% Title: %wt% `n, %logfile% 
		activeId = %idNew%
		activeTitle = %wt%
		break
	}
}
FileAppend, `n%A_Now%: Opening file: %fileInput%...`n, %logfile% 

; pen input file if provided (this is necessary due to some programs not opening files correctly via cmd)
; TODO if fileInput is relativ, get full path
; TODO flag to open like this or by cmd?

WinActivate, ahk_id %activeId%

Send, !f
Sleep, %SLEEP_TIMER%
Send, o
WinWait, Open
WinGetTitle, openTitle, Open
Sleep, %SLEEP_TIMER%
ControlGetFocus, openBox, %openTitle%
Sleep, %SLEEP_TIMER%
ControlSetText, %openBox%, %fileInput%, %openTitle%
Sleep, %SLEEP_TIMER%
SetControlDelay -1
ControlClick,&Open,%openTitle%
SetControlDelay 50

Sleep, 7000

;MsgBox, Continuing...


FileAppend, %A_Now%: Checking export types...`n, %logfile% 

Send, !f
Sleep, %SLEEP_TIMER%
Send, a
Sleep, %SLEEP_TIMER%

WinGetTitle, title, Save
ControlSend,,!t,%title%
Sleep, %SLEEP_TIMER%
ControlGetFocus, TypeBox, %title%
Sleep, %SLEEP_TIMER%
ControlGet, Items, List,,%TypeBox%, %title%
WinClose, Save

FoundMatch := false

Loop, Parse, Items, `n 
{
	;MsgBox %A_LoopField%
	SearchString := A_LoopField
	FoundPos := 0
	Length := StrLen(SearchString)

	FileAppend, %A_Now%: Found File Types: %A_LoopField%`n, %logfile% 

	while (FoundPos < Length){
		FoundPos++
		FoundPos := RegExMatch(SearchString, "\.[\w-\.]+", MatchType, FoundPos)
		;MsgBox %MatchType%
		
		if (FoundPos == 0){
			break
		}
		else if (MatchType = outputFormat){
			;MsgBox % "Found Match: " MatchType
			FoundMatch := true
			FileAppend, %A_Now%: Saving As %MatchType% `n, %logfile% 
			SaveFileAs(SearchString, MatchType)
		}
	}
	
}

if (!FoundMatch)
{
	FileAppend, %A_Now%: Did not find matching type - saving As All Files `n, %logfile% 
	SaveFileAs("All Files", outputFormat)
}


Sleep 1000

FileAppend, %A_Now%: Closing Window...`n, %logfile% 

WinClose, ahk_id %activeId%
FileAppend, %A_Now%: Exiting...`n, %logfile% 

ExitApp 0

SaveFileAs(checkBoxString, typeEnding) {
	
	global activeId
	global fileInput
	global newName
	global SLEEP_TIMER
	
	;MsgBox,,, "InFileSaveAs " %typeEnding%, 0.3
	
	Sleep, %SLEEP_TIMER%

	Send, !f
	Sleep, %SLEEP_TIMER%
	Send, a
	Sleep, %SLEEP_TIMER%
	WinWait, Save

	WinGetTitle, title, Save
	ControlSend,,!t,%title%
	Sleep, %SLEEP_TIMER%

	;sgBox, b

	ControlGetFocus, TypeBox, %title%
	Sleep, %SLEEP_TIMER%

	;MsgBox, c

	Control, ChooseString, %checkBoxString%, %TypeBox%, %title%
	Sleep, %SLEEP_TIMER%
	
	;MsgBox, d
	
	storePath := newName . typeEnding 
	
	ControlSetText, Edit1, %storePath%, %title% ;TODO check if this is always edit1
	
	;MsgBox, e

	
	Sleep, %SLEEP_TIMER%
	
	WinGet, OldId, ID, Save
	;MsgBox % "Before: " OldId
	
	SetControlDelay -1
	ControlClick,&Save,%title%
	SetControlDelay 50
	Sleep, %SLEEP_TIMER%
	
	WinGet, NewId, ID, Save
	;MsgBox % "After: " NewId

	If(NewID = "")
	{
		;MsgBox "id was blank, no overwrite needed"
		Sleep, %SLEEP_TIMER%
		return
	}
	else
	{
		;WinGet, ActiveControlList, ControlList, ahk_id %NewId%
		SetControlDelay -1
		ControlClick, &Yes, ahk_id %NewId% ;todo parameter? --overwrite?
		SetControlDelay 50
	}
	
	
	Sleep, %SLEEP_TIMER%
	return 
}

;Need this, because we can't work with active windows (not usable with win XP and NSSM)
WindowAlreadyExists(winId, len) {
	
	global AllIds
	WinGetTitle nt, ahk_id %winId%
	Loop %len%
	{
		MsgBox % "hmmmmmmm: " AllIds%A_Index%
		if (AllIds%A_Index% = winId){
			return A_Index
		}
	}
	return 0
}





	


