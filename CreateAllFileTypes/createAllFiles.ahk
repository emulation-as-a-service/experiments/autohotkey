#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%
#SingleInstance,Force
#WinActivateForce
SetControlDelay 50
SetKeyDelay 20
SetTitleMatchMode 2
DetectHiddenWindows, OFF

logfile = C:\all_types_log.txt
FileDelete %logfile%
#Include ../lib/logging.ahk

WriteLog("Starting All Types Exporter")

prog = %1%
fileInput = %2%
storeDirectory = %3%
saveExport = %4%

SLEEP_TIMER = %5%

INITIAL_DELAY = %6%


WriteLog("Got inputs:" prog . " " . fileInput . " " . storeDirectory . " " . saveExport " " . SLEEP_TIMER . " " . INITIAL_DELAY)

; SLEEP_TIMER = 2200

IfInString, fileInput, :\
{
	WriteLog("Input was given as absolut path: " .  fileInput)
}
else{
	fileInput = %A_ScriptDir%\%fileInput%
	WriteLog("Input was given as relative path. WorkingDir will be appended: " . fileInput)
}

SplitPath, fileInput,,dir,,baseName

; TODO do this (for input
if(!(storeDirectory="")){
	
	IfNotExist, %3%
		FileCreateDir, %3%
	
	StringRight, toCheck, storeDirectory, 1
	
	if(!(toCheck="\")){
	storeDirectory := storeDirectory . "\"
	}
	
	newName :=  storeDirectory . baseName 
}
else{
	newName := baseName
}


#Include ../lib/checkActiveWindows_activeId.ahk

#Include ../lib/openFile.ahk
openFile(fileInput, activeId, SLEEP_TIMER)

Sleep, %SLEEP_TIMER%
Sleep, %SLEEP_TIMER%

#Include ../lib/checkSaveAs_Items.ahk

WriteLog("Starting Main Program...")


ItemIndex := 0

; Main Loop
Loop, Parse, Items, `n 
{
	WriteLog("Item: " . A_LoopField) 

	dot = .

	IfInString, A_LoopField, %dot%
	{
		WriteLog("Got Item with '.', will apply regex to find types!")
		;MsgBox % A_LoopField
		SearchString := A_LoopField
		FoundPos := 0
		Length := StrLen(SearchString)

		while (FoundPos < Length){
			FoundPos++
			FoundPos := RegExMatch(SearchString, "\.[\w-\.]+", MatchType, FoundPos)
			;MsgBox %MatchType%
			if (FoundPos == 0){
				break
			}
			else{
			WriteLog("Saving As " . MatchType) 
			SaveExportFileAs(SearchString, MatchType, activeId, fileInput, newName, SLEEP_TIMER, saveExport, ItemIndex, true)
			ItemIndex++
			}
		}
	}
	else{
		WriteLog("Item does not specify ending with a '.' - will apply first three letters:")
		StringLeft, typeEnding, A_LoopField, 3
		
		MatchType :=  "." . typeEnding 
		WriteLog("Passing: " . MatchType)
		
		SaveExportFileAs(A_LoopField, MatchType, activeId, fileInput, newName, SLEEP_TIMER, saveExport, ItemIndex, false)
		ItemIndex++
	}
	
}

Sleep 1000

WriteLog("Closing Window...") 

WinClose, ahk_id %activeId% ; TODO force this? autocad for e.g. thinks there are changes after export -> press yes/no
WriteLog("Exiting...") 

ExitApp 0

#Include ../lib/saveExportFile.ahk

^y::
WriteLog("Ctrl + Y was pressed, terminating file Dump!")
ExitApp, 0
	

	


