
openFile(fileInput, activeId, SLEEP_TIMER){
	WinActivate, ahk_id %activeId%

	Send, !f
	Sleep, %SLEEP_TIMER%
	Send, o
	WinWait, Open,,5
	if(ErrorLevel){
		WriteLog("Could not find Window with 'Open' in the title, trying 'Select'...")
	}
	WinWait, Select,,5
	if(ErrorLevel){
		WriteLog("Could not find Window with 'Select' in the title, continuing anyway...")
	}
	WinGetTitle, openTitle, A
	Sleep, %SLEEP_TIMER%
	ControlGetFocus, openBox, %openTitle%
	Sleep, %SLEEP_TIMER%
	ControlSetText, %openBox%, %fileInput%, %openTitle%
	Sleep, %SLEEP_TIMER%
	SetControlDelay -1
	ControlClick,&Open,%openTitle%
	SetControlDelay 50

	Sleep, %SLEEP_TIMER%
}