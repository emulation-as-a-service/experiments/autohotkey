#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%
#SingleInstance,Force
SetControlDelay 0


prog = %1%
file = %2%

convertType = %3%
fileEnding = %4%


;prog = C:\Program Files\Sublime Text 3\sublime_text.exe
;prog = C:\Program Files (x86)\Notepad++\notepad++.exe
;file = tojson.txt
;convertType := "json"

;MsgBox % "my ahk version: " A_AhkVersion

Run %prog% %file%

; TODO probably just use this: WinMenuSelectItem, A,, File, Save As
Sleep 500
;TODO get active Window ID
WinGet, originalWinID, ID, A
Send !f
Sleep 200
Send a
Sleep 200

;showActive()
Send !t
Sleep 500

ControlGetFocus, TypeBox, A ;This should (always?) be ComboBox2, but I am doing this extra step to assure it 
ControlGet, Items, List,, %TypeBox%, A


If (%convertType% != "all")
{

	Control, ChooseString, %convertType%, %TypeBox%, A
	Sleep, 100
	
	if (ErrorLevel) 
	{
		ControlGet, typeText, Choice, , %TypeBox%, A
		IfInString, typeText, %convertType%
		{
			;MsgBox, "Successfully set type to" %typeText%
		}
		else
		{
			MsgBox, "Error: Could not find type " %convertType%
		}
	}
	
}
else
{ ; User passed "all" as convertType, meaning the file type is changed by extension only (not by selecting a type in the sub menu)
		MsgBox, "In all files!"

	allFilesFound := false

	Loop, Parse, Items, `n
	{
		
		IfInString, A_LoopField, "all type" or IfInStr, A_LoopField, "all file"
		{
			
			Control, Choose, %A_Index%, %TypeBox%, A
			allFilesFound := true
			break
		}
		
	}
	

	If not allFilesFound
	{
		MsgBox, "Error: Could not find a All Files or All Types field!",,"Error"
		return
	}
	
}	
; set text to newname
; TODO search this, instead of relying on Edit1 (see above, Alt + N)
Sleep 500
ControlClick, Edit1, A,,,1
Sleep 500


SplitPath, file,,,,basename

If (fileEnding =""){
	newname := basename ; if no file type is specified just store without old ending
}
else{
	newname := basename . "." . fileEnding ; when a type is specified, use it (necessary for programs, where one type option still allows for multiple types)
}

Send %newname%

Sleep 500
Send !s
Sleep 500


while true ;TODO add max iteration 
{
	WinGet, newId, ID, A
	If (originalWinID = newId) ; original window is active, we can close it
	{
		;MsgBox, Original Window Active - Done!
		WinClose, A
		return
	}
	else
	{
		;MsgBox, Other Window is active!
		WinGet, ActiveControlList, ControlList, A
		Loop, Parse, ActiveControlList, `n
		{
			ControlGetText, CText, %A_LoopField%, A
			
			;MsgBox, WinText %CText%
			
			relevantStrings := "yes ok confirm continue next &yes &ok &confirm &continue &next" ;TODO make configurable? find cleaner solution for &
			
			IfInString, relevantStrings, %CText% 
			{
				ControlClick, %A_LoopField%, A	
			}
			
		}

	}
	Sleep, 200
}

return
; TODO potential error handling (e.g. notepad++ throws error if save as already exists)

Esc::ExitApp
