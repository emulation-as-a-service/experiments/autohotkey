#NoEnv
SendMode Input
#SingleInstance, Force
sleep 10000

logfile = C:\autostart_log.txt
FileDelete %logfile%
#Include ../lib/logging.ahk

WriteLog("Starting Autostarter")


;TODO only CDROM ? Floppy Disks?
DriveGet, AllDrives, List, CDROM

WriteLog("Looping over Drives...")

Loop, Parse, AllDrives
{
	WriteLog("Checking Drive: " . A_LoopField)
	fileName = %A_LoopField%:\__eaas_autostart.txt

	if FileExist(fileName){
		WriteLog("Found __eaas_autostart.txt!")
		FileRead, ToBeExe, %fileName%
		SetWorkingDir %A_LoopField%:\
		WriteLog("Executing: " . ToBeExe)
		RunWait %ToBeExe%
		Shutdown, 1
	}
}


ExitApp 0

^q::
WriteLog("Ctrl + Q was pressed, terminating starter!")
ExitApp, 0
	