#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%
#SingleInstance,Force
#WinActivateForce
SetControlDelay 50
SetKeyDelay 20
SetTitleMatchMode 2
DetectHiddenWindows, OFF

SetRegView, 64
RegWrite, REG_SZ, HKLM, SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon, DefaultPassword, admin

if(A_Is64bitOS)
	MsgBox % "64 bit"