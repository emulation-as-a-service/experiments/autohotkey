Sleep, 5000

WinGet hWnd, ID, A

hMenu := DllCall("GetMenu", "UInt", hWnd)

#wholeMenu =
GetMenu(hMenu)

Clipboard = %#wholeMenu%
MsgBox %#wholeMenu%
Return

GetMenu(_hMenu, _menuLevel=0)
{
	local menuItemCount, indent, nPos, length, lpString, id, hSubMenu

	menuItemCount := DllCall("GetMenuItemCount", "UInt", _hMenu)
	indent =
	If _menuLevel > 0
	{
		indent := "|   "
		Loop % _menuLevel - 1
		{
			indent := indent "|   "
		}
		indent := indent "+-- "
	}
	Loop %menuItemCount%
	{
		nPos := A_Index - 1
		length := DllCall("GetMenuString"
			, "UInt", _hMenu
			, "UInt", nPos
			, "UInt", 0	; NULL
			, "Int", 0	; Get length
			, "UInt", 0x0400)	; MF_BYPOSITION
		VarSetCapacity(lpString, length + 1)	; I don't check the result...
		length := DllCall("GetMenuString"
			, "UInt", _hMenu
			, "UInt", nPos
			, "Str", lpString
			, "Int", length + 1
			, "UInt", 0x0400)
		id := DllCall("GetMenuItemID", "UInt", _hMenu, "Int", nPos)
		If length > 0	; 0 if not of MFT_STRING type or error
		{
			If id < 0
			{
				If _menuLevel = 0
					id = _____
				Else
					id = 00000
			}
			Else
			{
				id = 00000%id%
			}
			StringRight id, id, 5
			#wholeMenu = %#wholeMenu%%id% - %indent%%lpString%`n
		}
		hSubMenu := DllCall("GetSubMenu", "UInt", _hMenu, "Int", nPos)
		If hSubMenu != 0
		{
			GetMenu(hSubMenu, _menuLevel + 1)
		}
	}
}