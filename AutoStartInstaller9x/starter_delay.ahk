#NoEnv
SendMode Input
#SingleInstance, Force
sleep 10000

;TODO only CDROM ? Floppy Disks?
DriveGet, AllDrives, List, CDROM

Loop, Parse, AllDrives
{
	fileName = %A_LoopField%:\__eaas_autostart.txt

	if FileExist(fileName){
		FileRead, ToBeExe, %fileName%
		SetWorkingDir %A_LoopField%:\
		RunWait %ToBeExe%
		Shutdown, 1
	}
}


ExitApp 0
	